#!/usr/bin/env python

import os
import sys

def main():
    minSize = int(sys.argv[1])
    minRT = int(sys.argv[2])
    inFile = sys.argv[3]

    if not os.path.exists(inFile):
        raise

    seqName = None
    seqs = {}
    with open(inFile) as fhd:
        for line in fhd:
            linec = line.strip()

            if len(linec) == 0:
                continue

            if linec[0] == ">":
                seqName = linec[1:]
                seqs[seqName] = []
                print "parsing seq {}".format(seqName)

            else:
                if seqName is None:
                    raise

                cols = [int(x) for x in linec.split(" ")]
                
                #print len(cols), cols

                seqs[seqName].extend( cols )

    for seqName in seqs:
        print "seq {} # bp {}".format( seqName, len(seqs[seqName]) )

    with open("{}.gff3".format(inFile), 'w') as fhd:
        #poses = {}
        fhd.write("##gff-version 3\n")

        pid = 0

        for seqName, seq in seqs.iteritems():
            #poses[seqName] = []
            startPos = 0
            endPos = 0
            for pos, val in enumerate(seq):
                if val == 0 or val > minRT:
                    if ( endPos - startPos ) >= minSize:
                        pid += 1
                        seqLen = endPos - startPos + 1
                        print "PASSED. ID {:12,d} LEN {:12,d} START {:12,d} END {:12,d}".format( pid, seqLen, startPos, endPos )
                        #poses[seqName].append( (pid, seqLen, startPos, endPos) )
                        fhd.write("\t".join([str(x) for x in [ seqName, ".", "mRNA", startPos, endPos, ".", "+", ".", "ID=target_{};Name=target_{};pid={};length={}".format(pid, pid, pid, seqLen)  ]]) + "\n")
                    startPos = pos
                endPos = pos
        #print poses



if __name__ == "__main__":
    main()

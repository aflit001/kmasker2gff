#!/bin/bash

set -xeu

DB=$1
INFA=$2
INGFF=$3

OUTBN=${INFA}.blastn
OUTGFF=${INGFF}.blastn.gff
OUTGFF1=${OUTGFF}.single.gff
#DB=S_lycopersicum_chromosomes.2.50.fa
#INFA=S_lycopersicum_chromosomes.2.50.fa.gz_kmasked_RT03_3kb_N2X.fa

if [[ ! -f "${OUTBN}" ]]; then
blastn -task megablast -evalue 1e-20 -max_target_seqs 10 -num_threads 10 -outfmt 7 -db $DB -query $INFA -out $OUTBN
fi


if [[ ! -f "${OUTGFF}" ]]; then
pypy ./parse_blastn.py ${OUTBN} ${INGFF}
fi

if [[ ! -f "${OUTGFF1}" ]]; then
grep -E '#|matches=1$' ${OUTGFF} > ${OUTGFF1}
fi

#!/usr/bin/env python

import os
import sys
from collections import defaultdict

"""
$ cat blast_seqs.sh
#!/bin/bash

set -xeu

DB=$1
INFA=$2

OUTBN=${INFA}.blastn

#DB=S_lycopersicum_chromosomes.2.50.fa
#INFA=S_lycopersicum_chromosomes.2.50.fa.gz_kmasked_RT03_3kb_N2X.fa

if [[ ! -f "${OUTBN}" ]]; then
blastn -task megablast -evalue 1e-20 -max_target_seqs 10 -num_threads 10 -outfmt 7 -db $DB -query $INFA -out $OUTBN
fi
"""

def main():
    inblastn, ingff = sys.argv[1:]
    matches = defaultdict(int)
    mnum    = 0
    bln     = 0
    with open(inblastn, 'r') as fhd:
        for line in fhd:
            bln  += 1
            line  = line.strip()

            if len(line) == 0:
                continue

            if line[0] == "#":
                continue

            """
# BLASTN 2.2.26+
# Query: SL2.50ch06_18 3121
# Database: S_lycopersicum_chromosomes.2.50.fa
# Fields: query id, subject id, % identity, alignment length, mismatches, gap opens, q. start, q. end, s. start, s. end, evalue, bit score
# 2 hits found
SL2.50ch06_18   SL2.50ch06      100.00  3121    0       0       1       3121    3206    6326    0.0     5764
SL2.50ch06_18   SL2.50ch07      88.24   408     19      22      1       386     8702973 8703373 4e-127   460
            """

            mnum += 1

            #https://www.ncbi.nlm.nih.gov/books/NBK279675/
            qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore  = line.split("\t")
            pident, evalue = [float(f) for f in pident, evalue]
            length, mismatch, gapopen, qstart, qend, sstart, send, bitscore = [int(float(i)) for i in length,  mismatch, gapopen, qstart, qend, sstart, send, bitscore]
            #print bln, mnum, qseqid, sseqid, pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore

            matches[ qseqid ] += 1

    print "got {:12,d} sequences in {:12,d} lines with {:12,d} matches".format( len(matches), bln, mnum )

    with open(ingff + '.blastn.gff', 'w') as fhdo:
        snum   = 0
        gfn    = 0
        data   = {}
        with open(ingff, 'r') as fhdi:
            for line in fhdi:
                gfn  += 1
                line  = line.strip()
                #print line

                if len(line) == 0:
                    fhdo.write("\n")
                    continue

                if line[0] == "#":
                    fhdo.write(line + "\n")
                    continue

                """
##gff-version 3
SL2.50ch06      .       mRNA    3206    6327    .       +       .       ID=target_1;Name=target_1;pid=1;length=3121;inputFile=S_lycopersicum_chromosomes.2.50.fa.gz_kmasked_RT03_3kb_N2X.fa;seqName=SL2.50ch06_18 3121;serialId=18
                """

                cols    = line.split("\t")
                cdata   = dict([pair.split("=") for pair in cols[8].split(";")])
                #print cdata

                seqId, seqLen = cdata['seqName'].split()
                #print "seqId {} seqLen {:12,d}".format( seqId, int(seqLen) )

                assert seqId in matches, "sequence {} not in matches".format(seqId)

                k = (cols[0], int(cols[3]))
                if k not in data:
                    data[k] = []
                #print k
                #assert k not in data, "position {:12,d} in chromosome {} already exists".format(int(cols[3]), cols[0])
                data[ k ].append( line + ";matches={}\n".format(matches[seqId]) )

        for k, lines in sorted(data.iteritems()):
            for line in lines:
                fhdo.write( line )



if __name__ == '__main__':
    main()

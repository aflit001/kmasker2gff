#!/usr/bin/env python
"""
Given a reference FASTA file and a set of FASTA files exported by kmasker,
this script exports a GFF file with the coordinates of the kmasker's
sequences

Copyright (c) 2016, Saulo Alves Aflitos, Wageningen University
License: MIT
"""

import os
import sys
import argparse

from collections import OrderedDict

def readFasta(inRef):
    """
    Reads fasta file, placing the sequence in a ordered dict
    """

    refs = OrderedDict()

    with open(inRef) as ifhd:
        seqName = None
        for line in ifhd:
            line = line.strip()

            if len(line) == 0:
                continue

            elif line[0] == ">":
                seqName = line[1:]
                refs[seqName] = []
                print "adding ref #{:6,d} {}".format(len(refs), seqName)

            elif seqName is not None:
                refs[seqName] += [line]

    print "  merging sequence"

    totalLen = 0
    for seqName, seq in refs.iteritems():
        refs[seqName] = "".join(refs[seqName])
        totalLen += len(refs[seqName])
        print "  seq {} length {:12,d}".format(seqName, len(refs[seqName]))

    print "  sequence read. {:12,d} bp".format(totalLen)

    return refs

parser = argparse.ArgumentParser(description='Read fasta reference, read sequences extracted by kmasker and export GFF with the coordinates.')
parser.add_argument('--ref'  , metavar='REF'    , dest='ref'    , required=True, nargs=1                 , type=str, help='Reference file')
parser.add_argument('--out'  , metavar='OUT'    , dest='out'    , required=True, nargs=1                 , type=str, help='Output GFF file')
parser.add_argument('INFILES', metavar='INFILES',                                nargs=argparse.REMAINDER,           help='Input Files (kmasker output)')

def main():
    """
    Reads reference fasta
    Reads each kmasker fasta and generates gff from it by getting the substring index in the reference sequence
    """

    args = parser.parse_args()

    inRef = args.ref[0]
    outFile = args.out[0]
    inFiles = args.INFILES

    print "ref", inRef
    print "out", outFile
    print "files", len(inFiles), ", ".join(inFiles)

    if os.path.exists(outFile):
        print "output file {} exists. quitting".format(outFile)
        sys.exit(1)

    if not os.path.exists(inRef):
        print "input file {} does not exists".format(inRef)
        sys.exit(1)

    if len(inFiles) == 0:
        print "no input file given"
        sys.exit(1)

    if any([x == inRef for x in inFiles]):
        print "reference {} in list of files: {}".format(inRef, ", ".join(inFiles))
        sys.exit(1)


    print "reading reference"
    refs = readFasta(inRef)

    print "reading sequences"

    with open(outFile, 'w') as fhdo:
        fhdo.write("##gff-version 3\n")
        pid = 0
        for inFile in inFiles:
            print "  reading", inFile
            fileSeqs = readFasta(inFile)

            for seqName, seq in fileSeqs.iteritems():
                seqNameId, seqNameLen = seqName.split(" ")
                shortName, seqId = seqNameId.split("_")
                seqNameLen = int(seqNameLen)
                seqId = int(seqId)

                print "    seq Name {} ID {:12,d} len {:12,d}".format(shortName, seqId, seqNameLen)

                assert len(seq) == seqNameLen

                if shortName not in refs:
                    print "  sequence {} does not exists in reference".format( shortName )
                    raise

                else:
                    refSeq = refs[shortName]
                    pos = refSeq.index(seq)

                    if pos == -1:
                        print "  sequence {} ({}) in file {} not found".format(seqName, shortName, inFile)
                        raise

                    else:
                        seqLen = len(seq)
                        startPos = pos + 1
                        endPos = startPos + seqLen
                        pid += 1
                        print "  sequence {} ({}) in file {}. start {:12,d} len {:12,d} end {:12,d}".format(seqName, shortName, inFile, startPos, seqLen, endPos)
                        fhdo.write("\t".join([str(x) for x in [ shortName, ".", "mRNA", startPos, endPos, ".", "+", ".", "ID=target_{};Name=target_{};pid={};length={};inputFile={};seqName={};serialId={}".format(pid, pid, pid, seqLen, inFile, seqName, seqId) ]]) + "\n")
                    

if __name__ == "__main__":
    main()

#!/usr/bin/env python

import os
import sys


def parseFasta(fasta):
    fa = OrderedDict()

    with open(fasta, 'r') as fhd:
        lastKey = None
        for line in fhd:
            line = line.strip()

            if len(line) == 0:
                continue

            if line[0] == ">":
                lastKey = line[1:]
                fa[lastKey] = []

            else:
                fa.append(line)

    for k in fa:
        fa[k] = "".join(fa[k])

    return fa

def parseGff(fa, gff):
    with open(gff, 'r') as fhd:
        for line in fhd:
            line = line.strip()
            if len(line) == 0:
                continue

            if line[0] == "#":
                continue

            

def main():
    fasta, gff = sys.argv[1:]

    fa = parseFasta(fasta)
    paseGff(fa, gff)


if __main__ = "__main__":
    main()

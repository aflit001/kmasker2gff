Copyright (c) 2016, Saulo Alves Aflitos, Wageningen University
License: MIT


Given a reference FASTA file and a set of FASTA files exported by kmasker,
this script exports a GFF file with the coordinates of the kmasker's
sequences

for help:

```bash
python matcher.py -h
```

to run:

```bash
python matcher.py --ref REF.fasta --out OUT.gff IN1.fasta IN2.fasta IN3.fasta
```

The output GFF will look like:

```bash
##gff-version 3
SL2.50ch06      .       mRNA    3206    6327    .       +       .       ID=target_1;Name=target_1;pid=1;length=3121;inputFile=S_lycopersicum_chromosomes.2.50.fa.gz_kmasked_RT03_3kb_N2X.fa;seqName=SL2.50ch06_18 3121;serialId=18
```

Where:

```bash
ID = Serial number
length = length of the sequence
inputFile = input fasta file name
seqName = sequence name in the kmasker fasta
serialId = kmasker serial ID for the sequence
```
